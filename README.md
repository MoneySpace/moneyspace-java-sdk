# MoneySpace Java SDK v2

# Requirements

- Java JDK 17 or greater

# Dependencies

```xml

    <dependency>
      <groupId>net.moneyspace</groupId>
      <artifactId>moneyspace-sdk</artifactId>
      <version>2.0.0</version>
    </dependency>

```

# API

![API Architecture](./api-arch.png)

## Initial MoneySpace API

```java
  import net.moneyspace.sdk.*;

  MoneyspaceAPIBuilder moneyspaceAPIBuilder = new MoneyspaceAPIBuilder("secretId"
    , "secretkey"
    , "successCallBackUrl"
    , "failCallBackUrl"
    , "cancelCallBackUrl");

```
## Start/Stop HTTP Callback Listener

```java

  // Start HTTP listener
  moneyspaceAPIBuilder.startWebhook("hostname"
        , 8088
        , (moneyspaceWebhookDto) -> {
          // TODO : programming to handle call back request from payment gateway.
        });

  // TODO : API requests

  // Stop HTTP listener server
  moneyspaceAPIBuilder.stopWebhook();

```

## Create Transaction CreditCard/QR Code (API)

```java
  import net.moneyspace.sdk.beans.*;
    
  int timeout = 60;
  CreateTransactionRequest request = new CreateTransactionRequest();
  request.setFirstname("Your firstname");
  request.setLastname("Your lastname");
  request.setEmail("Your email");
  request.setPhone("Your phone");
  request.setAmount(Your amount);
  request.setDescription("Your description");
  request.setAddress("Your address");
  request.setMessage("Your message");
  request.setFeeType(MerchantFeeType.INCLUDE);
  request.setOrderId("Your Order ID");
  request.setPaymentType(MerchantPaymentType.CARD);
  request.setAgreement(MerchantAgreement.ONE);
  CreateTransactionResponse resp = moneyspaceAPIBuilder.createTransaction(request, timeout);

```

## Create Transaction ID and mskey

```java
  import net.moneyspace.sdk.beans.*;
  
  int timeout = 60;
  CreateTransactionIDRequest request = new CreateTransactionIDRequest();
  request.setFirstname("Your firstname");
  request.setLastname("Your lastname");
  request.setEmail("Your email");
  request.setPhone("Your phone");
  request.setAmount(Your amount);
  request.setDescription("Your description");
  request.setAddress("Your address");
  request.setMessage("Your message");
  request.setFeeType(MerchantFeeType.INCLUDE);
  request.setOrderId("Your Order ID");
  request.setPaymentType(MerchantPaymentType.CARD);
  request.setAgreement(MerchantAgreement.ONE);
  CreateTransactionIDResponse resp = moneyspaceAPIBuilderImpl.createTransaction(request, timeout);

```

## Create Transaction Installment

```java
  import net.moneyspace.sdk.beans.*;
  
  int timeout = 60;
  CreateTransactionIDRequest request = new CreateTransactionIDRequest();
  request.setFirstname("Your firstname");
  request.setLastname("Your lastname");
  request.setEmail("Your email");
  request.setPhone("Your phone");
  request.setAmount(Your amount);
  request.setDescription("Your description");
  request.setAddress("Your address");
  request.setMessage("Your message");
  request.setFeeType(MerchantFeeType.INCLUDE);
  request.setOrderId("Your Order ID");
  request.setPaymentType(MerchantPaymentType.CARD);
  request.setAgreement(MerchantAgreement.ONE);
  request.setBankType(BankType.KTC);
  request.setStartTerm(3);
  request.setEndTerm(10);
  CreateTransactionResponse resp = moneyspaceAPIBuilderImpl.createTransactionInstallment(request, timeout);

```

### Merchant Fee types
Merchant absorbs MDR and interest (Subsidy)

- `INCLUDE`: include
- `EXCLUDE`: exclude

### Merchant Payment types

- `CARD`: card
- `QRNONE`: qrnone

** `CARD` option is available for both fee type `INCLUDE` and `EXCLUDE` but `QRNONE` option is availabled only when fee type is `EXCLUDE`. 

### Merchant Agreement

- When `1` is I acknowledge that I will NOT claim the refund or cancel the order.
- When `2` is I acknowledge that I will NOT claim the refund and If the good / service is NOT in good condition , I will contact the Seller within 7 days or follow Seller's refund policy.
- When `3` is I acknowledge that I will NOT claim the refund and If the good / service is NOT in good condition , I will contact the Seller within 14 days or follow Seller's refund policy.
- When `4` is I acknowledge that I will NOT claim the refund and If the good / service is NOT in good condition , I will contact the Seller within 30 days or follow Seller's refund policy.
- When `5` is I acknowledge that I will NOT claim the refund and If the good / service is NOT in good condition , I will contact the Seller within 60 days or follow Seller's refund policy.

### Bank Type

- `KTC`: Krungthai card
- `BAY`: Krungsri card , Central card , Tesco lotus card
- `FCY`: Krungsri First choice , Home pro card , Mega home card

## Check payment status by transaction ID

```java
  import net.moneyspace.sdk.beans.*;
    
  String transactionId = "?????????";
  int timeout = 60;
  TransactionStatusResponse resp = moneyspaceAPIBuilder.checkPaymentStatusByTransaction(transactionId, timeout);

```

## Check payment status by order ID

```java
  import net.moneyspace.sdk.beans.*;
    
  String orderId = "?????????";
  int timeout = 60;
  OrderStatusResponse resp = moneyspaceAPIBuilder.checkPaymentStatusByOrder(orderId, timeout);

```

## Check Order Status (API)

```java
  import net.moneyspace.sdk.beans.*;
  
  String orderId = "?????????";
  int timeout = 60;
  CheckOrderIDResponse resp = moneyspaceAPIBuilderImpl.checkOrderID(orderId, 60);

```

## Check Transaction Status (API)

```java
  import net.moneyspace.sdk.beans.*;
  
  String transactionId = "?????????";
  int timeout = 60;
  CheckPaymentResponse resp = moneyspaceAPIBuilderImpl.checkPayment(transactionId, 60);

```

## Cancel Payment QR Code (API)

```java
  import net.moneyspace.sdk.beans.*;
  
  String transactionId = "?????????";
  int timeout = 60;
  CancelPaymentResponse resp = moneyspaceAPIBuilderImpl.cancelPayment(transactionId, 60);

```

### Payment Status

- `OK`: creating a successful payment method
- `paysuccess`: payment success
- `fail`: payment fail
- `cancel`: payment canceled
