package net.moneyspace.sdk;

import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import net.moneyspace.sdk.beans.*;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

/**
 * Money Space Java SDK Test class
 * @author Money space company limited
 * @version 2.0.0
 *
 */
public class MoneyspaceSDKTest {

  @Rule
  public WireMockRule wireMockRule = new WireMockRule(options().port(8089).notifier(new ConsoleNotifier(true)));

  private MoneyspaceAPIBuilderImpl moneyspaceAPIBuilderImpl;

//  private final String host = "http://localhost:8088";
  private final String host = "https://ec7e-2001-fb1-84-7f57-bd33-4ee2-94b1-3ebb.ngrok.io";
  private final String successCallBackUrl = this.host + "/success";

  @Before
  public void init() throws Exception {
    final String secretId = "O9657315A7HL05584N67";
    final String secretKey = "5GCTNak7547enaec2NE34AM1E397cHx41iqq65ATE56P4XEBPu583Sm39f0wHdvT6jFKpM2SY26iqEjc";

    final String failCallBackUrl = this.host + "/fail";
    final String cancelCallBackUrl = this.host + "/cancel";

    // Simulate test
    wireMockRule.start();
    moneyspaceAPIBuilderImpl = new MoneyspaceAPIBuilderImpl(secretId
      , secretKey
      , this.successCallBackUrl
      , failCallBackUrl
      , cancelCallBackUrl);

  }

  @After
  public void destroy() {
    wireMockRule.stop();
  }

  @Test
  public void createTransaction() throws Exception {
    stubFor(post("/payment/CreateTransaction")
      .withHeader("Content-Type", containing("multipart/form-data"))
      .willReturn(ok()
        .withBody("[{\"status\":\"success\"" +
          ",\"transaction_ID\":\"MSTRFST000000255014\"" +
          ",\"link_payment\":\"https:\\/\\/stage.moneyspace.net\\/merchantapi\\/makepayment\\/linkpaymentcard" +
          "?locale=th&transactionID=MSTRFST000000255014&timehash=20211031223948&secreteID=46G8958QCJ5X2EG3KDFU" +
          "&hash=044e300a1561a699fd894a0731497170673cd539f7059e5347d9883a00d36b0e\"}]")));

    CreateTransactionRequest request = new CreateTransactionRequest();
    request.setFirstname("John");
    request.setLastname("test");
    request.setEmail("test@test.com");
    request.setPhone("0888888888");
    request.setAmount(1.56);
    request.setDescription("product 001");
    request.setAddress("11/83");
    request.setMessage("testpay");
    request.setFeeType(MerchantFeeType.INCLUDE);
    request.setOrderId("pdtest001");
    request.setFeeType(MerchantFeeType.INCLUDE);
    request.setOrderId("880000011");
    request.setPaymentType(MerchantPaymentType.CARD);
    request.setAgreement(MerchantAgreement.ONE);
    CreateTransactionResponse resp = moneyspaceAPIBuilderImpl.createTransaction(request, 60);
    System.out.println("Create Transaction Response:: " + resp);
    Assert.assertNotNull(resp);
  }

  @Test
  public void createTransactionMskey() throws Exception {
    stubFor(post("/CreateTransactionID")
      .withHeader("Content-Type", containing("multipart/form-data"))
      .willReturn(ok()
        .withBody("[{\"status\":\"success\"" +
          ",\"transaction_ID\":\"MSTRFST000000255014\"" +
          ",\"mskey\":\"MSKEY000000999999\"" +
          ",\"link_payment\":\"https:\\/\\/stage.moneyspace.net\\/merchantapi\\/makepayment\\/linkpaymentcard" +
          "?locale=th&transactionID=MSTRFST000000255014&timehash=20211031223948&secreteID=46G8958QCJ5X2EG3KDFU" +
          "&hash=044e300a1561a699fd894a0731497170673cd539f7059e5347d9883a00d36b0e\"}]")));

    CreateTransactionIDRequest request = new CreateTransactionIDRequest();
    request.setFirstname("John");
    request.setLastname("test");
    request.setEmail("test@test.com");
    request.setPhone("08888888881");
    request.setAmount(1.98);
    request.setDescription("product 001");
    request.setAddress("11/83");
    request.setMessage("testpay");
    request.setFeeType(MerchantFeeType.INCLUDE);
    request.setOrderId("pdtest001");
    request.setPaymentType(MerchantPaymentType.CARD);
    request.setAgreement(MerchantAgreement.ONE);
    request.setBankType(BankType.BAY);
    request.setStartTerm(6);
    request.setEndTerm(6);
    CreateTransactionIDResponse resp = moneyspaceAPIBuilderImpl.createTransaction(request, 60);
    System.out.println("Create Transaction Mskey Response:: " + resp);
    Assert.assertNotNull(resp);
  }

  @Test
  public void getPaymentLink() {
    String transactionId = "MSTRFST000000279865";
    System.out.println(moneyspaceAPIBuilderImpl.getPaymentLink(transactionId));
  }

  @Test
  public void createTransactionInstallment() throws Exception {
    stubFor(post("/payment/Createinstallment")
      .withHeader("Content-Type", containing("multipart/form-data"))
      .willReturn(ok()
        .withBody("[{\"status\":\"success\"" +
          ",\"transaction_ID\":\"MSTRFST000000255014\"" +
          ",\"link_payment\":\"https:\\/\\/stage.moneyspace.net\\/merchantapi\\/makepayment\\/linkpaymentcard" +
          "?locale=th&transactionID=MSTRFST000000255014&timehash=20211031223948&secreteID=46G8958QCJ5X2EG3KDFU" +
          "&hash=044e300a1561a699fd894a0731497170673cd539f7059e5347d9883a00d36b0e\"}]")));

    CreateTransactionIDRequest request = new CreateTransactionIDRequest();
    request.setFirstname("John");
    request.setLastname("test");
    request.setEmail("test@test.com");
    request.setPhone("0888888888");
    request.setAmount(3000.99);
    request.setDescription("product 001");
    request.setAddress("11/83");
    request.setMessage("testpay");
    request.setFeeType(MerchantFeeType.INCLUDE);
    request.setOrderId("pdtest002");
    request.setPaymentType(MerchantPaymentType.CARD);
    request.setAgreement(MerchantAgreement.ONE);
    request.setBankType(BankType.KTC);
    request.setStartTerm(3);
    request.setEndTerm(10);
    CreateTransactionResponse resp = moneyspaceAPIBuilderImpl.createTransactionInstallment(request, 60);
    System.out.println("Create Transaction Installment Response:: " + resp);
    Assert.assertNotNull(resp);
  }

  @Test
  public void checkPaymentStatusByTransaction() throws Exception {
    String transactionId = "MSKTCST000000279866";
    stubFor(post("/payment/transactionID")
      .withHeader("Content-Type", containing("multipart/form-data"))
      .willReturn(ok()
        .withBody("[{\"Amount\":\"999.999\",\"Transaction ID\":\""
          + transactionId + "\",\"Description\":\"Payment description by transaction id\",\"Status Payment\":\"Pending\",}]")));

    TransactionStatusResponse resp = moneyspaceAPIBuilderImpl.checkPaymentStatusByTransaction(transactionId, 60);
    System.out.println("Check payment status by transaction `"+transactionId+"` response:: " + resp);
    Assert.assertNotNull(resp);
  }

  @Test
  public void checkPaymentStatusByOrder() throws Exception {
    String orderId = "pdtest002";
    stubFor(post("/payment/OrderID")
      .withHeader("Content-Type", containing("multipart/form-data"))
      .willReturn(ok()
        .withBody("[{\"Amount\":\"999.999\",\"Order ID\":\""
          + orderId + "\",\"Description\":\"Payment description by order id\",\"Status Payment\":\"Pending\",}]")));

    OrderStatusResponse resp = moneyspaceAPIBuilderImpl.checkPaymentStatusByOrder(orderId, 60);
    System.out.println("Check payment status by order `"+orderId+"` response:: " + resp);
    Assert.assertNotNull(resp);
  }

  @Test
  public void checkOrderID() throws Exception {
    String orderId = "pdtest002";
    stubFor(post("/CheckOrderID")
      .withHeader("Content-Type", containing("multipart/form-data"))
      .willReturn(ok()
        .withBody("[{\"order id\":{\"amount\":\"999.999\",\"description\":\"Payment description by order id\",\"status\":\"Pending\"}}]")));

    CheckOrderIDResponse resp = moneyspaceAPIBuilderImpl.checkOrderID(orderId, 60);
    System.out.println("Check payment status by order `"+orderId+"` response:: " + resp);
    Assert.assertNotNull(resp);
  }

  @Test
  public void checkPayment() throws Exception {
    String transactionId = "MSKTCST000000279866";
    stubFor(post("/CheckPayment")
      .withHeader("Content-Type", containing("multipart/form-data"))
      .willReturn(ok()
        .withBody("[{\" transaction id\":{\"amount\":\"999.999\",\"description\":\"Payment description by order id\",\"status\":\"Pending\"}}]")));

    CheckPaymentResponse resp = moneyspaceAPIBuilderImpl.checkPayment(transactionId, 60);
    System.out.println("Check payment status by transaction `"+transactionId+"` response:: " + resp);
    Assert.assertNotNull(resp);
  }

  @Test
  public void cancelPaymentQRCode() throws Exception {
    String transactionId = "MSKTCST000000279866";
    stubFor(post("/merchantapi/cancelpayment")
      .withHeader("Content-Type", containing("multipart/form-data"))
      .willReturn(ok()
        .withBody("[{\"message\":\"Transaction id : MSTRF18000000XXXXXX Canceled\",\"status\":\"success\"}]")));

    CancelPaymentResponse resp = moneyspaceAPIBuilderImpl.cancelPayment(transactionId, 60);
    System.out.println("Cancel payment status by transaction `"+transactionId+"` response:: " + resp);
    Assert.assertNotNull(resp);
  }

  @Test
  public void successCallback() throws Exception {
    // Start HTTP listener server
    moneyspaceAPIBuilderImpl.startWebhook("localhost"
      , 8088
      , (moneyspaceWebhookDto) -> {
        System.out.println("Call back URL with `locale`: " + moneyspaceWebhookDto.getLocale());
        System.out.println("Call back URL with `store_name`: " + moneyspaceWebhookDto.getStoreName());
        System.out.println("Call back URL with `store_logo`: " + moneyspaceWebhookDto.getStoreLogo());
        System.out.println("Call back URL with `store_phone`: " + moneyspaceWebhookDto.getStorePhone());
        System.out.println("Call back URL with `name_customer`: " + moneyspaceWebhookDto.getNameCustomer());
        System.out.println("Call back URL with `datetime`: " + moneyspaceWebhookDto.getDatetime());
        System.out.println("Call back URL with `idpay`: " + moneyspaceWebhookDto.getIdpay());
        System.out.println("Call back URL with `name_merchant`: " + moneyspaceWebhookDto.getNameMerchant());
        System.out.println("Call back URL with `agreement`: " + moneyspaceWebhookDto.getAgreement());
        System.out.println("Call back URL with `description`: " + moneyspaceWebhookDto.getDescription());
        System.out.println("Call back URL with `amount`: " + moneyspaceWebhookDto.getAmount());
      });

    // Initial HTTP Request Configure
    RequestConfig config = RequestConfig.custom()
      .setConnectTimeout(60 * 1000)
      .setConnectionRequestTimeout(60 * 1000)
      .setSocketTimeout(60 * 1000)
      .build();
    // Initial HTTP client with HTTP Request config by timeout.
    CloseableHttpClient httpClient = HttpClientBuilder.create()
      .setDefaultRequestConfig(config)
      .build();

    // Initial HTTP GET request with URL
    HttpPost httpPost = new HttpPost(this.successCallBackUrl + "?locale=th" +
      "&store_name=Demo%20Test" +
      "&store_logo=https%3A%2F%2Fmoneyspace-stage-bucket.ap-south-1.linodeobjects.com%2FST1028_20211102221623509.png" +
      "&store_phone=0888888855" +
      "&name_customer=Sarayut" +
      "&datetime=21-11-2021%2018%3A14" +
      "&idpay=MSTRFST000000267352" +
      "&name_merchant=%E0%B8%9A%E0%B8%88.%20%E0%B8%AA%E0%B8%AA%E0%B8%A7%E0%B8%AA%E0%B8%AA%20" +
      "&agreement=1" +
      "&description=Test%20Java%20SDK%20payment" +
      "&amount=0.99");
    CloseableHttpResponse httpResponse = httpClient.execute(httpPost);

    System.out.println("POST Response Status:: " + httpResponse.getStatusLine().getStatusCode());
    // Initial BufferedReader to read response payload contents
    BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
    String inputLine;
    StringBuilder response = new StringBuilder();
    while ((inputLine = reader.readLine()) != null) {
      response.append(inputLine);
    }
    reader.close();
    // print result
    System.out.println(response);
    httpClient.close();

    // Stop HTTP listener server
    moneyspaceAPIBuilderImpl.stopWebhook();
  }

}
