package net.moneyspace.sdk.beans;

/**
 * Money Space Java SDK Bank Type class
 * @author Money space company limited
 * @version 2.0.0
 *
 */
public enum BankType {

  KTC("KTC"),
  BAY("BAY"),
  FCY("FCY");

  public static final Integer[] KTCStartTerm = {3, 4, 5, 6, 7, 8, 9, 10};
  public static final Integer[] BAYStartTerm = {3, 4, 6, 9, 10};
  public static final Integer[] FCYStartTerm = {3, 4, 6, 9, 10};

  private final String label;

  BankType(String label) {
    this.label = label;
  }

  public String value() {
    return label;
  }

}
