package net.moneyspace.sdk.beans;

/**
 * Money Space Java SDK PaymentTransactionStatus class
 * @author Money space company limited
 * @version 2.0.0
 *
 */
public enum PaymentTransactionStatus {

  SUCCESS("success"),
  FAIL("fail"),
  CANCEL("cancel");

  private final String label;

  PaymentTransactionStatus(String label) {
    this.label = label;
  }

  public String value() {
    return label;
  }

}
