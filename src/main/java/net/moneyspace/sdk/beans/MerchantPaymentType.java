package net.moneyspace.sdk.beans;

/**
 * Money Space Java SDK Payment Type class
 * @author Money space company limited
 * @version 2.0.0
 *
 */
public enum MerchantPaymentType {

  CARD("card"),
  QRNONE("qrnone");

  private final String label;

  MerchantPaymentType(String label) {
    this.label = label;
  }

  public String value() {
    return label;
  }

}
