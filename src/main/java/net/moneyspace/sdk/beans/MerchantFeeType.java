package net.moneyspace.sdk.beans;

/**
 * Money Space Java SDK Fee Type class
 * @author Money space company limited
 * @version 2.0.0
 *
 */
public enum MerchantFeeType {

  INCLUDE("include"),
  EXCLUSE("exclude");

  private final String label;

  MerchantFeeType(String label) {
    this.label = label;
  }

  public String value() {
    return label;
  }
}
