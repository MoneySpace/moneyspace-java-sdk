package net.moneyspace.sdk.beans;

/**
 * Money Space Java SDK Agreement class
 * @author Money space company limited
 * @version 2.0.0
 *
 */
public enum MerchantAgreement {

  ONE(1),
  TWO(2),
  THREE(3),
  FOUR(4),
  FIVE(5);

  private final int val;

  MerchantAgreement(int val) {
    this.val = val;
  }

  public int value() {
    return val;
  }
}
