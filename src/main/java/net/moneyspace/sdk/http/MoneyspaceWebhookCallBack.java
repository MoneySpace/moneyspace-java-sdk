package net.moneyspace.sdk.http;

import net.moneyspace.sdk.beans.MoneyspaceWebhookDto;

/**
 * Money Space Java SDK HTTP WebHook Callback class
 * @author Money space company limited
 * @version 2.0.0
 */
public interface MoneyspaceWebhookCallBack {

  /**
   * HTTP Call Back handle method
   * @param moneyspaceWebhookDto data transfer object bean to retreive value from call back HTTP request
   * @throws Exception all potential exception
   */
  void handle(MoneyspaceWebhookDto moneyspaceWebhookDto) throws Exception;

}
